// Cria o sequelize
const Sequelize = require("sequelize");

// cria uma conexão com o banco, passando o nome do banco , user , senha
const connection = new Sequelize('guiaPerguntas','seuUsuario','suaSenha',{
    host:'localhost',
    dialect: 'mysql'
});

module.exports = connection;